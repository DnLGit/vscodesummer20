package geometry;

import java.util.ArrayList;

class TriangleEqui extends Triangle {

    public TriangleEqui(ArrayList<Double> sides){                       // vedi commenti su Telegram del 17 settembre ...
        super(sides);
    }

/*     public double getLato(){                                         // perche' qui l'else mi elimina l'eventuale errore?
        if(sides.get(0) == sides.get(1) && sides.get(1) == sides.get(2))                        // e non serve fare confronti
            return sides.get(0);                                                            // tanto i lati sono tutti uguali
        else return sides.get(0);                                                           // vedi sotto metodo corretto ...
    } */

    public double getLato(){    
        return sides.get(0);
    
}

    public double getHeight(){
        double lato = getLato();
        return Math.sqrt(Math.pow(lato, 2) - Math.pow(lato/2, 2));

    }

    public double computeArea(){            // overRide del metodo "computeArea".
        return this.getLato()*this.getHeight()/2;

    }
    
}
