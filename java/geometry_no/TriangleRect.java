package geometry_no;

import java.util.ArrayList;     // [[aggiunta libreria per ArrayList]]
import java.util.Math;

class TriangleRect extends Triangle{

    public TriangleRect(ArrayList<Double> sides){        // [[costruttore di prima]] 
        super(sides);                                   // [[perche' non c'e' base e altezza?]]
                                                        // [[a cosa mi serve questo 3??]]
    } 

    
    public double getCatetomin(){
        if(sides.get(0) <= sides.get(1) && sides.get(0) <= sides.get(2))
            return sides.get(0);

        if(sides.get(1) <= sides.get(0) && sides.get(1) <= sides.get(2))
            return sides.get(1);
        
        if(sides.get(2) <= sides.get(0) && sides.get(2) <= sides.get(1))
            return sides.get(2);         
    }

    public double getCathLow(){                         // [[ alternativa per cateto minore ]]
        double cathLow = sides.get(0);
        for(int i=1; i<sides.size(); i++){
            if(sides.get(i) < cathLow)
                cathLow = sides.get(i);
        }
        return cathLow;
    }

    public double getCatetoMin2(){
        return Math.min( Math.min(sides.get(0),sides.get(1)), sides.get(2));
    }

    public double getCatetomag(){
        if(sides.get(0) <= sides.get(1) && sides.get(0) >= sides.get(2))
            return sides.get(0);

        if(sides.get(0) >= sides.get(1) && sides.get(0) <= sides.get(2))
            return sides.get(0);

        if(sides.get(1) <= sides.get(0) && sides.get(1) >= sides.get(2))
            return sides.get(1);
        
        if(sides.get(1) >= sides.get(0) && sides.get(1) <= sides.get(2))
            return sides.get(1);
        
        if(sides.get(2) <= sides.get(0) && sides.get(2) <= sides.get(1))
            return sides.get(2);
        
        if(sides.get(2) >= sides.get(0) && sides.get(2) >= sides.get(1))
            return sides.get(2);
    }

    public double getCatetomag2(){                         // [[ alternativa per cateto maggiore ]]
        double ip = getIpotenusa2();
        if(ip == sides.get(0))
            return Math.max(sides.get(1), sides.get(2));
        
        else if(ip == sides.get(1))
            return Math.max(sides.get(0), sides.get(2));
        
        else
            return Math.max(sides.get(0), sides.get(1));
    }



    public double getipotenusa(){
        if(sides.get(0) > sides.get(1) && sides.get(0) > sides.get(2))
            return sides.get(0);

        if(sides.get(1) > sides.get(0) && sides.get(1) > sides.get(2))
            return sides.get(1);
        
        if(sides.get(2) > sides.get(0) && sides.get(2) > sides.get(1))
            return sides.get(2);         
    }

    public double getIpotenusa2(){
        return Math.max( Math.max(sides.get(0),sides.get(1)), sides.get(2));
    }

    public double getHipo(){                             // [[ alternativa per ipotenusa ]]
        double hipo = sides.get(0);
        for(int i=1; i<sides.size(); i++){
            if(sides.get(i) > hipo)
                hipo = sides.get(i);
        }
        return hipo;
    }


    //implement abstract method of super class
    public double computeArea(){
        return this.getCatetomin()*this.getCatetomag()/2;
    }

    public double computeArea2(){                       // [[ alternativa per calcolo Area  ]]
        return this.getCathLow()*this.getCathHig()/2;
    }    

}