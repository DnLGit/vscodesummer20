package geometry_no;

import java.util.ArrayList;

class Test{

    public static void main(String[] args){

        System.out.println("\n***** ArrayList e Poligoni *****\n");
       
        ArrayList<Double> sidesPar = new ArrayList<>();

        sidesPar.add(50.0);
        sidesPar.add(25.0);
        sidesPar.add(50.0);
        sidesPar.add(25.0);
        
        Parallelogram pr = new Parallelogram( sidesPar, 50.0, 25.0);

        System.out.println("Perimetro Parallelogram: " + pr.computePerimeter());

        System.out.println("Area Parallelogram: "+ pr.computeArea());
        
        System.out.println();
        ArrayList<Double> sidesTri = new ArrayList<>();

        sidesTri.add(50.0);
        sidesTri.add(25.0);
        sidesTri.add(25.0);
       

        Triangle tr = new Triangle( sidesTri, 50.0, 22);

        System.out.println("Perimetro Triangle: " + tr.computePerimeter());

        System.out.println("Area Triangle: "+ tr.computeArea());
        
        return;
    }
}